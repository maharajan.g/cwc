//
//  Group.swift
//  CWC
//
//  Created by Mac on 02/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
/// Module for Group
struct Group {
    var name: String
    var member: String
    
    init(gName: String, gMember: String) {
        self.name = gName
        self.member = gMember
    }
    
}
