//
//  UserCredential.swift
//  CWC
//
//  Created by Mac on 14/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

struct UserCredential {
    
    let oauthToken: String
    let OauthTokenSecret: String
    init(oauthtoken:String, oauthTokensecret:String){
        self.oauthToken = oauthtoken
        self.OauthTokenSecret = oauthTokensecret
    }
    
}

