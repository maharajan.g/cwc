//
//  OauthService.swift
//  CWC
//
//  Created by Mac on 18/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import OAuthSwift
/// Authrization service with flickr using OAuthSwift
class OauthService:OAuthWebViewControllerDelegate{
    
    static let shared = OauthService()
    let oauthswift = OAuth1Swift(
        consumerKey:    ConsumerKeys.consumerKey,
        consumerSecret: ConsumerKeys.consumerSecret,
        requestTokenUrl: "https://www.flickr.com/services/oauth/request_token",
        authorizeUrl:    "https://www.flickr.com/services/oauth/authorize",
        accessTokenUrl:  "https://www.flickr.com/services/oauth/access_token"
    )
    let userdefault = UserDefaults.standard
    let services = Services()
    let DocumentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let FileManager: FileManager = Foundation.FileManager.default
    lazy var internalWebViewController: WebViewController = {
        let controller = WebViewController()
        controller.view = UIView(frame: UIScreen.main.bounds) // needed if no nib or not loaded from storyboard

        controller.delegate = self
        controller.viewDidLoad() // allow WebViewController to use this ViewController as parent to be presented
        return controller
    }()
     init() {}
    func doOAuthFlickr(completion: @escaping (Bool) -> ()){
        oauthswift.authorizeURLHandler = getURLHandler()
        //oauthswift.accessTokenBasicAuthentification = true
        let _ = oauthswift.authorize(
        withCallbackURL: URL(string: "oauth-swift://oauth-callback/flickr")!) { result in
            switch result {
            case .success(let (credential, _, parameters)):
                let userId = parameters["user_nsid"] as! String
                let u_id = userId.removingPercentEncoding
                self.userdefault.set(u_id, forKey: UserDefaultKeys.userId)
                self.userdefault.set(credential.oauthToken, forKey: UserDefaultKeys.oauth_token)
                self.userdefault.set(credential.oauthTokenSecret, forKey: UserDefaultKeys.oauth_secret)
                completion(true)
                   
                case .failure(let error):
                    print(error.description)
                }
            }
        }    
    /// Url Hander for api request
    func getURLHandler() -> OAuthSwiftURLHandlerType {
        return internalWebViewController
    
        }
    // Configuration path setup
    var confPath: String {
        let appPath = "\(DocumentDirectory)/.oauth/"
        if !FileManager.fileExists(atPath: appPath) {
            do {
                try FileManager.createDirectory(atPath: appPath, withIntermediateDirectories: false, attributes: nil)
            }catch {
                print("Failed to create \(appPath)")
            }
        }
        return "\(appPath)Services.plist"
    }
    func initConf() {
        //let _ = internalWebViewController.webView
        initConfOld()
        print("Load configuration from \n\(self.confPath)")

        // Load config from model file
        if let path = Bundle.main.path(forResource: "Services", ofType: "plist") {
            services.loadFromFile(path)

            if !FileManager.fileExists(atPath: confPath) {
                do {
                    try FileManager.copyItem(atPath: path, toPath: confPath)
                }catch {
                    print("Failed to copy empty conf to\(confPath)")
                }
            }
        }
        services.loadFromFile(confPath)
        
    }
    func initConfOld() { // TODO Must be removed later

        services["Flickr"] = Flickr

    }
    // OAuthWebViewControllerDelegate required functions
    func oauthWebViewControllerDidPresent() {
        
    }

    func oauthWebViewControllerDidDismiss() {
        
    }

    func oauthWebViewControllerWillAppear() {
        
    }

    func oauthWebViewControllerDidAppear() {
        
    }

    func oauthWebViewControllerWillDisappear() {
        
    }

    func oauthWebViewControllerDidDisappear() {
        
    }
}
/*
1. AccessPlist file
2. Access key & print it
3. Process the key
*/
