//
//  CollectionCell.swift
//  CWC
//
//  Created by Mac on 19/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

/// Photo CollectionCellView 
class CollectionCell: UICollectionViewCell {
    private lazy var activityIndicator: UIActivityIndicatorView = {
       let activityindicator = UIActivityIndicatorView(style: .medium)
        activityindicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityindicator.startAnimating()
        return activityindicator
    }()
    var imageurl: String?{
    didSet{
        if let url = imageurl {
            URLSession.shared.dataTask(with: URL(string: url)! as URL, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    print(error ?? "No Error")
                    return
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = UIImage(data: data!)
                    self.activityIndicator.removeFromSuperview()
                    self.imageView.image = image
                })
            }).resume()
        }
    }
    }
    var imageView: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        //imageView.isUserInteractionEnabled = true
        return imageView
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(activityIndicator)
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: bounds.size.height).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
