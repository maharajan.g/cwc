//
//  GroupCell.swift
//  CWC
//
//  Created by Mac on 02/11/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
/// TableCellView for Group List of GroupName and GroupMembers
class GroupCell: UITableViewCell{
    private lazy var activityIndicator: UIActivityIndicatorView = {
       let activityindicator = UIActivityIndicatorView(style: .medium)
        activityindicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityindicator.startAnimating()
        return activityindicator
    }()
    var group: Group?{
    didSet{
        if let name = group?.name {
            nameLabelView.text = name
            self.activityIndicator.removeFromSuperview()
        }
        if let members = group?.member{
            membersCountLabel.text = "\(members) Members"
        }

    }
}
    private lazy var nameLabelView: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
//    private lazy var groupIcon: UIImageView = {
//        let icon = UIImageView()
//        icon.translatesAutoresizingMaskIntoConstraints = false
//        icon.backgroundColor = UIColor.green
//        icon.image = UIImage(named: "group")
//        return icon
//    }()
    private lazy var membersCountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(activityIndicator)
        contentView.addSubview(nameLabelView)
        contentView.addSubview(membersCountLabel)
        viewSetUp()
        
    }
    func viewSetUp(){
        NSLayoutConstraint.activate([
        
            nameLabelView.topAnchor.constraint(equalTo: contentView.topAnchor),
            nameLabelView.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: 10),
            nameLabelView.widthAnchor.constraint(equalToConstant: 300),
            nameLabelView.heightAnchor.constraint(equalToConstant: 30),

            membersCountLabel.topAnchor.constraint(equalTo: nameLabelView.bottomAnchor),
            membersCountLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: 10),
            membersCountLabel.widthAnchor.constraint(equalToConstant: 200),
            membersCountLabel.heightAnchor.constraint(equalToConstant: 20)
        
        ])
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}

