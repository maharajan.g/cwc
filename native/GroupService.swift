//
//  GroupService.swift
//  CWC
//
//  Created by Mac on 21/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import OAuthSwift
class GroupService{
    static let instance = GroupService()
    let url = "https://api.flickr.com/services/rest/"
    var oauthToken = UserDefaults.standard.value(forKey: UserDefaultKeys.oauth_token) as! String
    var uid = UserDefaults.standard.value(forKey: UserDefaultKeys.userId) as! String
    var groupSet: [Group] = []
    
    /// Fetch function for GroupList from flickr api
    ///  parameters must of oauth_token and user_id and in  signIn State
    /// - Parameter completion: GroupList
    func FetchListOfGroups(completion: @escaping (Array<Any>) -> ()){
    //  print(OauthService.shared.oauthswift!)Array<String><String>
        //print(oauthToken!)
        let parameters :Dictionary = [
            "method"         : "flickr.people.getGroups",
            "api_key"        : ConsumerKeys.consumerKey,
            "oauth_token"    : oauthToken,
            "user_id"        : uid,
            "format"         : "json",
            "nojsoncallback" : "1"
        ]
        let _ = OauthService.shared.oauthswift.client.get(url, parameters: parameters) { result in
              switch result {
              case .success(let response):
                let jsonDict = try? response.jsonObject() as? NSDictionary
                let group: NSArray
                let groups = jsonDict!["groups"] as! [String:AnyObject]
                group = groups["group"]  as! NSArray
                self.groupSet.removeAll()
                for imageData in group{
                    let data = imageData as! [String:AnyObject]
                    let name = data["name"] as! String
                    let member = data["members"] as! String
                    let groupList = Group.init(gName: name, gMember: member)
                    self.groupSet.append(groupList)
                }
              case .failure(let error):
                    print("Failed to fetch groups")
                  print(error)
              }
            completion(self.groupSet)
            print("Sucess")
      }
    }
}
