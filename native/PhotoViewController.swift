//
//  PhotoViewController.swift
//  CWC
//
//  Created by Mac on 15/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

///  ViewController for Photoist
class PhotoViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    var photosUrlArray = [String]()
//    private lazy var flowLayout: CollectionLayout = {
//        let layout = CollectionLayout()
//        layout.scrollDirection = .vertical
//        return layout
//    }()
    private lazy var collectionView: UICollectionView = {
        let collectionview = UICollectionView(frame: .zero, collectionViewLayout:UICollectionViewFlowLayout())
    self.view.addSubview(collectionview)
    collectionview.backgroundColor = UIColor.white
        
    collectionview.dataSource = self
    collectionview.delegate = self
    collectionview.register(CollectionCell.self, forCellWithReuseIdentifier: "cell")
    collectionview.translatesAutoresizingMaskIntoConstraints = false
    return collectionview
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        CollectionViewSetUp()
        PhotosServices.instance.FetchPhoto(completion: ({imagedata in
            self.photosUrlArray = imagedata
            self.collectionView.reloadData()
        } ) )
    }
    func CollectionViewSetUp(){
        self.view.backgroundColor = UIColor.white        
        navigationController?.navigationItem.title = "Groups"
        self.view.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.photosUrlArray.count
        //return 7
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellView = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionCell
        cellView.layer.borderColor = UIColor.white.cgColor
        cellView.layer.borderWidth = 1
        cellView.contentMode = .scaleAspectFill
        cellView.clipsToBounds = true
        cellView.imageurl = self.photosUrlArray[indexPath.row]
        //self.collectionView.reloadData()
        return cellView
    }
    func collectionView(_ collectionView: UICollectionView,
                    layout collectionViewLayout: UICollectionViewLayout,
                    sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height / 3)
    }
}

