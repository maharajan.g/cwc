//
//  PhotosServices.swift
//  CWC
//
//  Created by Mac on 16/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import OAuthSwift
import Foundation

///  Service Class for PhotoList  fetch function
class PhotosServices {
    
    static let instance =  PhotosServices()
    let userdefault = UserDefaults.standard
    var imageUrlSet = [String]()
    var uId: String = UserDefaults.standard.value(forKey: UserDefaultKeys.userId) as! String
    let url = "https://api.flickr.com/services/rest/"
    
    /// Fetch Action for PhotoList from flickr api
    /// - Parameter completion: PhotoList
    func FetchPhoto(completion: @escaping (Array<String>) -> ()){
      //  print(OauthService.shared.oauthswift!)
        let parameters :Dictionary = [
            "method"         : "flickr.photos.search",
            "api_key"        : ConsumerKeys.consumerKey,
            "user_id"        : uId,
            "format"         : "json",
            "nojsoncallback" : "1",
            "extras"         : "url_q,url_z"
        ]
        let _ = OauthService.shared.oauthswift.client.get(url, parameters: parameters) { result in
                switch result {
                case .success(let response):
                    let jsonDict: NSDictionary
                    jsonDict = try! response.jsonObject() as! NSDictionary
                    let photoList: NSArray
                    let photos = jsonDict["photos"] as! [String:AnyObject]
                    photoList = photos["photo"]  as! NSArray
                    self.imageUrlSet.removeAll()
                    for imageData in photoList{
                        let data = imageData as! [String:AnyObject]
                        self.imageUrlSet.append(data["url_q"] as! String)
                    }
                    completion(self.imageUrlSet)
                case .failure(let error):
                    print(error)
                }
        }
    }
}

