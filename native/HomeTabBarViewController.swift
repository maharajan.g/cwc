//
//  MainTabBarViewController.swift
//  CWC
//
//  Created by Mac on 22/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

/// HomeTabBarViewController  contains Photo and Group
class HomeTabBarViewController: UITabBarController {
    var tokens: UserCredential?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        let logoutBarButtonItem = UIBarButtonItem(title: "Logout", style: .done, target: self, action: #selector(logoutAction))
        self.navigationItem.rightBarButtonItem  = logoutBarButtonItem
        tabBar.barTintColor = UIColor(red: 38/255, green: 196/255, blue: 133/255, alpha: 1)
        setUpTabBar()
    }
    func setUpTabBar(){
        let photoViewController = (PhotoViewController())
        photoViewController.tabBarItem.title = "PhotoStream"
        photoViewController.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 22)], for: .normal)
        
        let groupViewController = GroupsViewController()
        groupViewController.tabBarItem.title = "Groups"
        groupViewController.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 22)], for: .normal)
        viewControllers = [photoViewController,groupViewController]
    }
    /// LogOut Action
    @objc func logoutAction(){
        //print(UserDefaults.standard.value(forKey: UserDefaultKeys.oauth_token)!)
        UserDefaults.standard.removeObject(forKey: UserDefaultKeys.oauth_token)
        UserDefaults.standard.removeObject(forKey: UserDefaultKeys.oauth_secret)
        UserDefaults.standard.removeObject(forKey: UserDefaultKeys.userId)
      //  print(UserDefaults.standard.value(forKey: UserDefaultKeys.oauth_token)!)
        URLCache.shared.removeAllCachedResponses()
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        //let startUpVc = GetStartUpViewController()
        self.navigationController?.popViewController(animated: true)
        //need to pop or root view like window
    }
}
