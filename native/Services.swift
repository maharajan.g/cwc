//
//  Services.swift
//  CWC
//
//  Created by Mac on 13/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
typealias ServicesValue = String

/// Service Class for flickr authorization
class Services {
    var parameters : [String: [String: ServicesValue]]

    init() {
       self.parameters = [:]
    }

    subscript(service: String) -> [String:ServicesValue]? {
        get {
            return parameters[service]
        }
        set {
            if let value = newValue , !Services.parametersEmpty(value) {
                parameters[service] = value
             }
        }
    }

    func loadFromFile(_ path: String) {
        if let newParameters = NSDictionary(contentsOfFile: path) as? [String: [String: ServicesValue]] {
            for (service, dico) in newParameters {
                if parameters[service] != nil && Services.parametersEmpty(dico) {
                    continue
                }
            }
        }
    }
    
    static func parametersEmpty(_ dico: [String: ServicesValue]) -> Bool {
       return  Array(dico.values).filter({ (p) -> Bool in !p.isEmpty }).isEmpty
    }




}
