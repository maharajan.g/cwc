//
//  SignInViewController.swift
//  CWC
//
//  Created by Mac on 12/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import OAuthSwift

/// This class is used for sign into Flickr and HomePage of Application
class GetStartUpViewController: OAuthViewController {
    var oauthswift: OAuthSwift?
    let userdefault = UserDefaults.standard
    var imageUrlSet = [String]()
    let services = Services()
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor.white
        label.textAlignment = .center
        label.text = "OAuth Verification"
        label.font = UIFont.boldSystemFont(ofSize: CGFloat(25))
        return label
    }()
    private lazy var signInButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Sign In with Flickr", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.layer.borderWidth = 1
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(signAuthentication), for: .touchUpInside)
        return button
    }()
    
}

extension GetStartUpViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true)
        view.addSubview(textLabel)
        view.addSubview(signInButton)
        setUpConstraints()
        self.view.backgroundColor = UIColor.white
        // Load config from files
        OauthService.shared.initConf()
        let _ = OauthService.shared.internalWebViewController.webView
    }
    override func viewWillAppear(_ animated: Bool) {
        OauthService.shared.initConf()
        let _ = OauthService.shared.internalWebViewController.webView
    }
    func setUpConstraints(){
        NSLayoutConstraint.activate([
        
            textLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            textLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            //textLabel.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: UIScreen.main.bounds.width / 4),
            textLabel.widthAnchor.constraint(equalToConstant: 300),
            textLabel.heightAnchor.constraint(equalToConstant: 50),
            
            signInButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            //signInButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: UIScreen.main.bounds.width / 4),
            signInButton.widthAnchor.constraint(equalToConstant: 200),
            signInButton.heightAnchor.constraint(equalToConstant: 50),
            signInButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -50)
        
        ])
    }
    @objc func signAuthentication(){
            OauthService.shared.doOAuthFlickr(completion: ({result in
                if result == true{
                    self.homeTabBarController()
                }else {
                    print("failed to fetch keys")
                }
                
            } ) )
    }
    func homeTabBarController(){
        let tabBarcontroler = HomeTabBarViewController()
        //HomeTabBarViewController().tokens = OauthService.shared.userTokens
        self.navigationController?.pushViewController(tabBarcontroler, animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}
