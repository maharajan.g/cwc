//
//  GroupsViewController.swift
//  CWC
//
//  Created by Mac on 22/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit

class GroupsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var group: [Group] = []
     private lazy var tableView: UITableView = {
     let tableview = UITableView(frame: .zero, style: .grouped)
     tableview.translatesAutoresizingMaskIntoConstraints = false
     tableview.delegate = self
     tableview.dataSource = self
     return tableview
     }()
     override func viewDidLoad() {
         super.viewDidLoad()
         view.addSubview(tableView)
         tableviewSetUp()
        // fetch function call for GroupList as compeltion
        GroupService.instance.FetchListOfGroups(completion: ({data in
            self.group = data as! [Group]
            self.tableView.reloadData()
        } ) )
    }
    /// Setup for TableView
    func tableviewSetUp(){
        navigationItem.title = "Groups"
        view.backgroundColor = UIColor.lightGray
        NSLayoutConstraint.activate([
            
           tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
           tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
           tableView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
           tableView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height)
            
            ])
        tableView.register(GroupCell.self, forCellReuseIdentifier: "GroupCell")
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Groups"
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return group.count
     }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath) as? GroupCell
        tableViewCell?.group = self.group[indexPath.row]
        return tableViewCell!
     }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
     }

}
