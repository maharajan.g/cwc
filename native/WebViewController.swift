//
//  WebViewController.swift
//  CWC
//
//  Created by Mac on 13/10/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import UIKit
import OAuthSwift
import WebKit

typealias WebView = UIWebView
/// ViewController for flickr webView
class WebViewController: OAuthWebViewController {
    var targetURL: URL?
    let webView: WebView = WebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.webView.frame = UIScreen.main.bounds
        self.view.addSubview(self.webView)
        NSLayoutConstraint.activate([

            self.webView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 100),
            self.webView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0),
            self.webView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width),
            self.webView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height)
        ])
//        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-0-[view]-0-|", options: [], metrics: nil, views: ["view":self.webView]))
        loadAddressURL()
    }
    
    override func handle(_ url: URL) {
        targetURL = url
        super.handle(url)
        self.loadAddressURL()
    }
    func loadAddressURL() {
        guard let url = targetURL else {
            return
        }
        let req = URLRequest(url: url)
        DispatchQueue.main.async {
            self.webView.loadRequest(req)
            
        }
    }
    
}
extension WebViewController: WKNavigationDelegate {

func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
    
    // here we handle internally the callback url and call method that call handleOpenURL (not app scheme used)
    if let url = navigationAction.request.url , url.scheme == "oauth-swift" {
        //AppDelegate.sharedInstance.applicationHandle(url: url)
        decisionHandler(.cancel)
        
        self.dismissWebViewController()
        return
    }
    
    decisionHandler(.allow)
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("\(error)")
        self.dismissWebViewController()
        // maybe cancel request...
    }
}
